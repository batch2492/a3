

//In JS, classes can be created using the "class" keyword



// Naming convention for classes : Begin with Uppercase Characters


/*

	Syntax:

	class <Name> {
	


	}
*/


// here we have an empty student class


// Instantiation - process of creating objects from class
// To create an object from a class, use the "new" keyword, when a class has a constructor, we need to supply ALL the values needed by the constructor.




class Person {

	constructor (name, age, nationality, address){

		this.name = name;
		age >= 18 && typeof age == 'number' ? this.age = age : this.age = undefined;
		this.nationality = nationality;
		this.address = address;

	}
}


let personOne = new Person('joe', 20 , 'filipino', 'batangas')

let personTwo = new Person('jane', 16, 'filipino', 'laguna')


console.log(personOne);
console.log(personTwo);



// Activity 1 QUIZ

/*
1. Class
2. Uppercase character first letter 
3. constructor
4. Instantiation
5. Constructor method
*/



//  Activity 1 Function Coding


class Student {

	constructor (name, email, grades){

		// properties
		this.name = name;
		this.email = email;

		this.gradeAve = undefined;
		this.pass = undefined;
		this.honor = undefined;

		grades.length == 4 && grades.every(score => {
			return score <= 100 && score > 0;
		}) ? this.grades = grades : this.grades = undefined;
	}



	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name} 's quartely grade averages are:  ${this.grades}`)
		return this;
	}
	computeAve(){

		let sum = 0;
		this.grades.forEach(grade => sum += grade );
		// update property
		this.gradeAve = sum/this.grades.length;

		return this;
	}

	willPass(){
		this.pass = (this.gradeAve >= 85) ? true : false;
		return this;
	}

	willPassWithHonors(){
		this.honor = this.pass && this.gradeAve >= 90 ? true : undefined
		return this;
	}

}

let studentOne = new Student('John','john@mail.com',[89, 84, 78, 88]);

let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);

let studentThree = new Student ('Jane', 'jane@mail.com', [87, 89, 91, 93]);

let studentFour = new Student ('Jessie', 'jessie@mail.com', [91, 89, 92, 93])



console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);


//getter and setter

// Best practice dictates that we regulate access to such properties. We do so, via the use of "getter" (regulates retrieval) and setters (regulates manipulation)


// Method Chaining


// Activity 2 Quiz

/*
1. No
2. No
3. Yes
4. getters and setters
5. this

*/

console.log(studentOne.login().computeAve().willPass().willPassWithHonors().logout());
console.log(studentTwo.login().computeAve().willPass().willPassWithHonors().logout());
console.log(studentThree.login().computeAve().willPass().willPassWithHonors().logout());
console.log(studentFour.login().computeAve().willPass().willPassWithHonors().logout());